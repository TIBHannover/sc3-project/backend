"""empty message

Revision ID: 095b4a157976
Revises: 4eb1067bbc1b
Create Date: 2022-07-07 16:45:05.100359

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '095b4a157976'
down_revision = '4eb1067bbc1b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('sc3_user_model', schema=None) as batch_op:
        batch_op.add_column(sa.Column('user_name', sa.String(), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('sc3_user_model', schema=None) as batch_op:
        batch_op.drop_column('user_name')

    # ### end Alembic commands ###
