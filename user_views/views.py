from flask import jsonify, request, json
from flask.views import MethodView
from util import use_args_with
from ._params import UserHeaderGetParams, ViewProfileArgs, UserProjectsGetParams, UserRoleArgs, UserEmailArgs
from models import UserModel, Role, UsersRoles, UsersProjects, ProjectModel
from functools import wraps
import requests


def requires_role(allowed_roles, *outer_args, **outer_kwargs):
    def wrapper(view_function, *wrapper_args, **wrapper_kwargs):
        for x in wrapper_args:
            print("OuterAgs:" + x)

        for x in wrapper_kwargs.values():
            print("Outer KAgs:" + x)

        @wraps(view_function)  # Tells debuggers that is is a function wrapper
        def decorator(*args, **kwargs):
            # user_manager = current_app.user_manager

            # check if user has role ???
            user_role = UserModel.get_user_role_for_id(outer_args[0])
            if user_role in allowed_roles:
                # It's OK to call the view
                return view_function(*args, **kwargs)
            else:
                # not okay to call the view_function
                return {"error": "Role not match"}

        return decorator

    return wrapper


class UserAPIRegister(MethodView):
    def post(self):
        result = UserModel.create_user(request.json)
        return jsonify(result)


class UserAPILogin(MethodView):

    def post(self):

        auth_type = request.json.get('auth_type')
        res = ''
        # local : email passwd login
        if auth_type == "AUTH_LOCAL":
            res = UserModel.find_or_login_user(
                {'email': request.json['username'], 'auth_type': request.json['auth_type'],
                 'passwd': request.json['password']})

        # ----------- CURRENTLY NOT USER AT ALL --------------
        # token based login
        if auth_type == "AUTH_TOKEN":
            # we assume this is a token based authorization
            res = UserModel.find_or_login_user({'token': request.json['token']})

        # GitHub auth
        if auth_type == "AUTH_GITHUB":
            # we assume this is a token based authorization
            user = UserModel.find_or_create_user(
                {'email': request.json['email'], 'display_name': request.json['displayName'],
                 'auth_type': request.json['auth_type'], 'email_valid': True})
            res = UserModel.find_or_login_user({'user_id': user['user_id'],
                                                'auth_type': request.json['auth_type']})

        # Gitlab auth
        if auth_type == "AUTH_GITLAB":
            # we assume this is a token based authorization
            user = UserModel.find_or_create_user(
                {'email': request.json['email'], 'display_name': request.json['displayName'],
                 'auth_type': request.json['auth_type'], 'email_valid': True})
            res = UserModel.find_or_login_user({'user_id': user['user_id'],
                                                'auth_type': request.json['auth_type']})
            
        # Google auth
        if auth_type == "AUTH_GOOGLE":
            # we assume this is a token based authorization
            user = UserModel.find_or_create_user(
                {'email': request.json['email'], 'display_name': request.json['displayName'],
                 'auth_type': request.json['auth_type'], 'email_valid': True})
            res = UserModel.find_or_login_user({'user_id': user['user_id'],
                                                'auth_type': request.json['auth_type']})
                        
        # SAP auth
        if auth_type == "AUTH_SAP":
            # we assume this is a token based authorization
            user = UserModel.find_or_create_user(
                {'email': request.json['email'], 'display_name': request.json['displayName'],
                 'auth_type': request.json['auth_type'], 'email_valid': True})
            res = UserModel.find_or_login_user({'user_id': user['user_id'],
                                                'auth_type': request.json['auth_type']})            
    
        return jsonify(res)
    
    
    # def _get_oauth_user_info(self, token, provider):
    #     """
    #     Helper method to fetch user info from an OAuth provider using the access token.
    #     """
    #     # Define provider-specific endpoints (add more providers as needed)
    #     provider_endpoints = {
    #         'sap': 'https://aqcbt5t42.accounts.ondemand.com/oauth2/userinfo',
    #         'google': 'https://www.googleapis.com/oauth2/v3/userinfo',
    #         'microsoft': 'https://graph.microsoft.com/v1.0/me',
    #         # Add more providers here
    #     }

    #     print(f"Fetching {provider} user info with token: {token[:10]}...")

    #     if not token or token == 'undefined':
    #         print("Invalid token received", flush=True)
    #         print(token, flush=True)
    #         return None
        
    #     # Format token properly
    #     auth_token = token if token.startswith('Bearer ') else f'Bearer {token}'

    #     headers = {
    #         'Authorization': auth_token,
    #         'Accept': 'application/json',
    #         'Content-Type': 'application/json'
    #     }

    #     try:
    #         print(f"Calling {provider} endpoint with headers: {headers}", flush=True)
    #         # Call the provider's user info endpoint
    #         response = requests.get(provider_endpoints[provider], headers=headers, verify=True)

    #         print(f"Response Status: {response.status_code}", flush=True)
    #         print(f"Response Headers: {response.headers}", flush=True)
    #         print(f"Response Body: {response.text}", flush=True)

    #         if response.status_code == 401:
    #             print("Authentication failed - invalid token", flush=True)
    #             return None

    #         response.raise_for_status()
    #         user_info = response.json()

    #         if provider == 'sap':
    #         # Map SAP-specific response fields
    #             return {
    #                 'email': user_info.get('email'),
    #                 'display_name': user_info.get('name', user_info.get('email')),
    #                 'sub': user_info.get('sub'),  # SAP unique identifier
    #             }

    #         return user_info  # Return user info (e.g., email, display name)
    #     except requests.exceptions.RequestException as e:
    #         print(f"Error fetching user info from {provider}: {str(e)}")
    #         print(f"Response status: {getattr(e.response, 'status_code', 'N/A')}")
    #         print(f"Response body: {getattr(e.response, 'text', 'N/A')}")
    #         return None


class UserDelete(MethodView):
    @use_args_with(UserHeaderGetParams)
    def get(self, reqargs):
        if reqargs.get("userId"):
            user_id = reqargs.get("userId")
            res = UserModel.delete_user(user_id)
            return res
        return jsonify({'error': "No user found"})


class UserHeader(MethodView):
    @use_args_with(UserHeaderGetParams)
    def get(self, reqargs):
        if reqargs.get("userId"):
            user_id = reqargs.get("userId")
            token = reqargs.get("token")
            res = UserModel.get_header_info_for_user(user_id, token)
            return jsonify(res)

        return jsonify({'error': "No user found"})


class AdminDashboard(MethodView):
    @use_args_with(UserHeaderGetParams)
    def get(self, reqargs):
        user_id = reqargs.get("userId")
        token = reqargs.get("token")
        print('is user id exist ?')
        allowed_roles = ["Admin", "System Admin", "Project Admin"]

        @requires_role(allowed_roles, user_id, token)
        def execute():
            users = UserModel.get_all_users_for_dashboard()
            if users:
                all_users = [{"uuid": user.uuid,
                              "auth_type": user.auth_type,
                              "display_name": user.display_name,
                              "email_valid": user.email_valid,
                              "email_address": user.email_address,
                              "role": user.roles[0].name
                              }
                             for user in users]
                return jsonify(all_users)
            else:
                return jsonify({'error': "Something went wrong"})

        return execute()

        #

        # default return


class ViewProfile(MethodView):
    @use_args_with(ViewProfileArgs)
    def get(self, reqargs):
        if reqargs.get("userId"):
            user_id = reqargs.get("userId")
            token = reqargs.get("token")

            res = UserModel.get_profile_info(user_id, token)

            return jsonify(res)

        return jsonify({'error': "No user found"})

    @use_args_with(ViewProfileArgs)
    def post(self, reqargs):
        if request.json:
            user_id = reqargs.get("userId")
            token = reqargs.get("token")

            return jsonify(UserModel.update_user_settings(user_id, token, request.json))
        else:
            return jsonify({"error": "no information updated"})


class UpdateUserRole(MethodView):
    @use_args_with(UserRoleArgs)
    def post(self, reqargs):

        if request.json:
            user_id = request.json["userId"]
            user_role = request.json["userRole"]
            return jsonify(UserModel.update_user_role(user_id, user_role))
        else:
            return jsonify({"error": "no information updated"})


class GetUserRole(MethodView):
    @use_args_with(UserRoleArgs)
    def get(self, reqargs):
        userUUID = reqargs["userId"]
        if userUUID:
            return jsonify(UserModel.get_user_role_for_id(userUUID))
        else:
            return jsonify({"error": "No role found for the user"})


class GetAllRoles(MethodView):
    @use_args_with(UserHeaderGetParams)
    def get(self, reqargs):
        user_id = reqargs.get("userId")
        token = reqargs.get("token")
        allowed_roles = ["Admin", "System Admin", "Project Admin"]

        @requires_role(allowed_roles, user_id, token)
        def execute():
            roles = Role.get_all_roles()
            if roles:
                all_roles = [{
                    "role": role.name,
                    "role_id": role.id
                }
                    for role in roles]
                return jsonify(all_roles)
            else:
                return jsonify({'error': "Something went wrong"})

        return execute()


class UpdateUserProjects(MethodView):
    @use_args_with(UserProjectsGetParams)
    def post(self, reqargs):

        if request.json:
            userId = request.json["userId"]
            projectsId = request.json["projectsId"]
            if userId:
                return UsersProjects.update_user_projects(userId, projectsId)
        else:
            return {
                "success": False,
                "error": "No user Project Combination Updated"
            }


class GetUserProjects(MethodView):
    @use_args_with(UserProjectsGetParams)
    def get(self, reqargs):
        userUUID = reqargs["userId"]
        user_id = UserModel.get_user_id_for_uuid(userUUID)

        if user_id:
            res = jsonify(UsersProjects.get_user_projects(user_id))
            return res
        return jsonify({'error': "No projects found for the user"})


class GetUserProjectsDetail(MethodView):
    @use_args_with(UserProjectsGetParams)
    def get(self, reqargs):
        userUUID = reqargs["userId"]
        user_id = UserModel.get_user_id_for_uuid(userUUID)

        if user_id:
            res = jsonify(UsersProjects.get_user_projects_detail(user_id))
            return res
        return jsonify({'error': "No projects found for the user"})


class GetProjectUsersDetail(MethodView):
    @use_args_with(UserProjectsGetParams)
    def get(self, reqargs):
        projectUUID = reqargs["projectId"]
        project_id = ProjectModel.get_project_id_for_uuid(projectUUID[0])

        if project_id:
            res = jsonify(UsersProjects.get_project_users(project_id))
            return res
        return jsonify({'error': "No projects found for the user"})


class EditEmailValid(MethodView):
    def post(self):
        if request.json:
            uuid = request.json["uuid"]

            UserModel.edit_email_valid(uuid)
            return jsonify({"result": True, "Edit": 'successful'})

        else:
            return jsonify({"result": False, "error": "no information updated"})


class EmailExists(MethodView):
    def post(self):
        if request.json:
            email = request.json["email_address"]
            res = UserModel.is_email_exists(email)
            return res

        return jsonify({"successful": False, "message": "something went wrong please try again after some time"})


class SetNewPassword(MethodView):
    def post(self):
        if request.json:
            user_id = request.json["user_id"]
            password = request.json["password"]

            res = UserModel.update_password(user_id, password)
            return res

        return jsonify({"success": False, "message": "something went wrong please try again after some time"})


class GetAllUsers(MethodView):
    def get(self):
        def execute():
            users = UserModel.get_all_users_for_dashboard()
            if users:
                all_users = [{"uuid": user.uuid,
                              "auth_type": user.auth_type,
                              "display_name": user.display_name,
                              "email_valid": user.email_valid,
                              "email_address": user.email_address,
                              "role": user.roles[0].name
                              }
                             for user in users]
                return jsonify(all_users)
            else:
                return jsonify({'error': "Something went wrong"})

        return execute()


class DeleteUserFromProject(MethodView):
    def get(self):
        if request.json:
            projectUUID = request.json['projectUUID']
            userUUID = request.json['userUUID']
            userRemoved = UsersProjects.delete_user_from_project(projectUUID, userUUID)
            return userRemoved
        return jsonify({'result': 'Failed'})


class CheckIfUserExists(MethodView):
    @use_args_with(UserEmailArgs)
    def get(self, reqargs):
        emailId = reqargs["emailId"]
        if emailId:
            doesEmailExist = UserModel.is_email_exists(emailId)
            return doesEmailExist
        return jsonify({'result': 'Failed'})


class AddUserToProject(MethodView):
    def get(self):
        if request.json:
            projectUUID = request.json['projectUUID']
            userUUID = request.json['userUUID']
            userAdded = UsersProjects.add_user_to_project(projectUUID, userUUID)
            return userAdded
        return jsonify({'result': 'Failed'})


class GetAllSystemAdmin(MethodView):
    def get(self):
        return UserModel.get_All_System_Admin()
