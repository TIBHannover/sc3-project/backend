from flask import Blueprint
from .views import UserAPIRegister, UserAPILogin, UserHeader, AdminDashboard, ViewProfile, UserDelete, GetAllRoles, \
    UpdateUserRole, UpdateUserProjects, GetUserProjects, GetUserRole, EditEmailValid, EmailExists, SetNewPassword, \
    GetAllUsers, GetUserProjectsDetail, GetProjectUsersDetail, DeleteUserFromProject, CheckIfUserExists, \
    AddUserToProject, GetAllSystemAdmin

users_blueprint = Blueprint("users", __name__)

users_blueprint.add_url_rule('/users/register/', view_func=UserAPIRegister.as_view('users_view'),
                             methods=['POST'])

users_blueprint.add_url_rule('/users/login/', view_func=UserAPILogin.as_view('users_login'),
                             methods=['POST'])

users_blueprint.add_url_rule('/users/edit_email_valid/', view_func=EditEmailValid.as_view('edit_user_email_valid'),
                             methods=['POST'])

users_blueprint.add_url_rule('/users/delete/', view_func=UserDelete.as_view('users_delete'),
                             methods=['GET'])

users_blueprint.add_url_rule('/users/email_exists/', view_func=EmailExists.as_view('email_exists'),
                             methods=['POST'])

users_blueprint.add_url_rule('/users/set_new_password/', view_func=SetNewPassword.as_view('set_new_password'),
                             methods=['POST'])

# TODO: change above methods to DELETE or POST

users_blueprint.add_url_rule('/users/header/', view_func=UserHeader.as_view('users_header'),
                             methods=['GET'])

users_blueprint.add_url_rule('/users/viewProfile/', view_func=ViewProfile.as_view('view_profile'),
                             methods=['GET', 'POST'])

users_blueprint.add_url_rule('/users/updateUserRole/', view_func=UpdateUserRole.as_view('update_user_role'),
                             methods=['POST'])

users_blueprint.add_url_rule('/admin/dashboard/', view_func=AdminDashboard.as_view('admin_dashboard'),
                             methods=['GET', 'POST'])

users_blueprint.add_url_rule('/roles/all/', view_func=GetAllRoles.as_view('roles'),
                             methods=['GET'])

users_blueprint.add_url_rule('/user/role/', view_func=GetUserRole.as_view('user_role'),
                             methods=['GET'])

users_blueprint.add_url_rule('/users/updateUserProjects/', view_func=UpdateUserProjects.as_view('update_user_projects'),
                             methods=['POST'])

users_blueprint.add_url_rule('/user/projects/', view_func=GetUserProjects.as_view('get_user_projects'),
                             methods=['GET'])


users_blueprint.add_url_rule('/user/projectsDetail/', view_func=GetUserProjectsDetail.as_view('get_user_projects_detail'),
                             methods=['GET'])

users_blueprint.add_url_rule('/project/usersDetail/', view_func=GetProjectUsersDetail.as_view('get_project_users_detail'),
                             methods=['GET'])


users_blueprint.add_url_rule('/users/all/', view_func=GetAllUsers.as_view('users_all'),
                             methods=['GET', 'POST']),

users_blueprint.add_url_rule('/project/unregisterUser/', view_func=DeleteUserFromProject.as_view('delete_user_from_project'),
                             methods=['GET'])

users_blueprint.add_url_rule('/project/addUser/', view_func=AddUserToProject.as_view('add_user_to_project'),
                             methods=['GET'])

users_blueprint.add_url_rule('/users/getAllSystemAdmin/', view_func=GetAllSystemAdmin.as_view('get_all_system_admin'),
                             methods=['GET'])

users_blueprint.add_url_rule('/user/doesUserExist/', view_func=CheckIfUserExists.as_view('is_email_exist'),
                             methods=['GET'])