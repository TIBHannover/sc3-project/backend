from extensions import db


# Define the Role data-model
class Role(db.Model):
    __tablename__ = 'sc3_role_model'
    id = db.Column('id', db.Integer(), primary_key=True)
    name = db.Column('name', db.String(50), unique=True)

    def __init__(self, role_name):
        self.name = role_name

    @classmethod
    def role_exists(cls, role_name):
        role = db.session.query(cls).filter_by(name=role_name).first()
        if role:
            return True
        else:
            return False

    @classmethod
    def get_all_roles(cls):
        return Role.query.order_by(Role.name).all()

    @classmethod
    def create_role(cls, role_name):
        # role exists?
        if not cls.role_exists(role_name):
            new_role = cls(role_name=role_name)
            db.session.add(new_role)
            db.session.commit()
            print("added new role")

    @classmethod
    def initialize(cls):
        Role.create_role("Public User")
        Role.create_role("Member")
        Role.create_role("System Admin")
        Role.create_role("Project Admin")


