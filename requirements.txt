#py2neo
numpy
flask
werkzeug
#tables
Flask-Cors
flask_sqlalchemy
sqlalchemy_utils
psycopg2-binary
marshmallow==3.0.0rc5
webargs==5.4.0
flask-marshmallow==0.10.1
BaseHash
Flask-Migrate
python-dotenv
passlib
flask_user
libgravatar
requests
